#include <cassert>
#include <tuple>
#include <vector>

#include <su3.h>

namespace su3
{

void wu39lm(int lm1, int mu1, int lm2, int mu2, int lm12, int mu12, int lm3, int mu3, int lm4,
            int mu4, int lm34, int mu34, int lm13, int mu13, int lm24, int mu24, int lm, int mu,
            std::vector<double>& su39lm) {
   int rho12max = su3::mult(lm1, mu1, lm2, mu2, lm12, mu12);
   int rho34max = su3::mult(lm3, mu3, lm4, mu4, lm34, mu34);
   int rho13max = su3::mult(lm1, mu1, lm3, mu3, lm13, mu13);
   int rho24max = su3::mult(lm2, mu2, lm4, mu4, lm24, mu24);
   int rho1234max = su3::mult(lm12, mu12, lm34, mu34, lm, mu);
   int rho1324max = su3::mult(lm13, mu13, lm24, mu24, lm, mu);
   size_t nsu39lm = rho12max * rho34max * rho13max * rho24max * rho1234max * rho1324max;

   assert(nsu39lm);

   su39lm.assign(nsu39lm, 0.0);

   size_t n1_a, n2_a, n3_a;
   size_t n1_b, n2_b, n3_b;
   size_t n1_c, n2_c, n3_c;

   std::vector<std::tuple<int, int, int>> ir0_vector;
   su3::couple(lm13, mu13, lm2, mu2, ir0_vector);

   n1_b = rho12max;

   std::vector<double> u6_a, u6_b, u6_c;
   u6_a.reserve(nsu39lm);
   u6_b.reserve(nsu39lm);
   u6_c.reserve(nsu39lm);

   for (auto ir0 : ir0_vector) {
      int rho132max, lm0, mu0;
      std::tie(rho132max, lm0, mu0) = ir0;

      int rho123max = su3::mult(lm12, mu12, lm3, mu3, lm0, mu0);
      if (!rho123max) {
         continue;
      }
      int rho04max = su3::mult(lm0, mu0, lm4, mu4, lm, mu);
      if (!rho04max) {
         continue;
      }

      wru3(lm13, mu13, lm2, mu2, lm, mu, lm4, mu4, lm0, mu0, lm24, mu24, rho132max, rho04max,
           rho24max, rho1324max, u6_a);
      wzu3(lm2, mu2, lm1, mu1, lm0, mu0, lm3, mu3, lm12, mu12, lm13, mu13, rho12max, rho123max,
           rho13max, rho132max, u6_b);
      wru3(lm12, mu12, lm3, mu3, lm, mu, lm4, mu4, lm0, mu0, lm34, mu34, rho123max, rho04max,
           rho34max, rho1234max, u6_c);

      size_t index_9lm = 0;

      n1_a = rho132max;
      n2_a = rho04max * n1_a;
      n3_a = rho24max * n2_a;

      n2_b = rho123max * n1_b;
      n3_b = rho13max * n2_b;

      n1_c = rho123max;
      n2_c = rho04max * n1_c;
      n3_c = rho34max * n2_c;

      for (size_t rho1324 = 0; rho1324 < rho1324max; ++rho1324) {
         for (size_t rho24 = 0; rho24 < rho24max; ++rho24) {
            for (size_t rho13 = 0; rho13 < rho13max; ++rho13) {
               for (size_t rho1234 = 0; rho1234 < rho1234max; ++rho1234) {
                  for (size_t rho34 = 0; rho34 < rho34max; ++rho34) {
                     for (size_t rho12 = 0; rho12 < rho12max; ++rho12, ++index_9lm) {
                        for (size_t rho04 = 0; rho04 < rho04max; ++rho04) {
                           size_t index_u6_a = rho1324 * n3_a + rho24 * n2_a + rho04 * n1_a; 
                           size_t index_u6_c = rho1234 * n3_c + rho34 * n2_c + rho04 * n1_c;
                           for (size_t rho132 = 0; rho132 < rho132max; ++rho132, ++index_u6_a) {
                              size_t index_u6_b = rho132 * n3_b + rho13 * n2_b + rho12;
                              for (size_t rho123 = 0; rho123 < rho123max; ++rho123) {
                                 su39lm[index_9lm] += u6_a[index_u6_a] * u6_b[index_u6_b] *
                                                      u6_c[index_u6_c + rho123];
                                 index_u6_b += n1_b;
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }
}

} // namespace su3
