#include <algorithm>
#include <cassert>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <limits> 
#include <tuple>
#include <vector>

extern "C"
{
#include <wigxjpf.h>
}

#include <su3.h>

namespace su3
{

double dwr3(int jj1, int jj2, int jj3, int mm1, int mm2, int mm3)
{
   int phase = ((-jj1 + jj2 - mm3) % 4) ? -1 : 1;
   return phase * std::sqrt(jj3 + 1) * wig3jj(jj1, jj2, jj3, mm1, mm2, -mm3);
}

std::tuple<int, int> GetIXIY(int I, int J, int lm, int mu, int L) {
   int iy;
   int ix = lm + mu + L;
   if (I == 0) {
      if (I == J) {
         ix = ix - mu;
      }
      iy = 6 * (lm + mu);
   } else {
      if (I == J) {
         ix = ix - lm;
      }
      iy = 0;
   }
   return std::make_tuple(ix, iy);
}

void ComputeGGEkLMbrackets(int ix, int I, int J, int lm, int mu, int ie, int jjt, int mmt, int K,
                           int L, int M, const std::vector<double>& O, std::vector<double>& dta) {
   int n = dta.size();
   size_t ii_index = 0;
   for (int ik = 0; ik < n; ++ik) {
      if (K == 0) {
         dta[0] = 0.0;
         if (ix != 0) {
            continue;
         }
      }
      dta[ik] = O[ii_index] * dtu3r3(I, J, lm, mu, ie, jjt, mmt, K, L, M);
      double ds = 0;
      size_t ji_index = ik;
      for (int jk = 0; jk < ik; ++jk) {
         ds += O[ji_index] * dta[jk];
         ji_index += n;
      }
      dta[ik] += ds;
      ii_index += (n + 1);
      K += 2;
   }
}

void xwu3r3(int I1, int J1, int lm1, int mu1, int L1, const std::vector<double>& O1, int I2, int J2,
            int lm2, int mu2, int L2, const std::vector<double>& O2, int I3, int J3, int lm3,
            int mu3, int L3, const std::vector<double>& O3, int k3max, int k2max, int k1max,
            int rhomax, const std::vector<double>& dewu3,
            const std::vector<std::tuple<int, int, int>>& j1ta_j2ta_iea,
            std::vector<double>& su3cgs) {
   int k3start = Kmin(I3, J3, lm3, mu3, L3);
   int ie3, j3t, m3t;
   std::tie(ie3, j3t, m3t) = GetExtremalStateLabels(I3, J3, lm3, mu3);

   int k2start = Kmin(I2, J2, lm2, mu2, L2);
   int i2x, i2y;
   std::tie(i2x, i2y) = GetIXIY(I2, J2, lm2, mu2, L2);

   int k1start = Kmin(I1, J1, lm1, mu1, L1);
   int i1x, i1y;
   std::tie(i1x, i1y) = GetIXIY(I1, J1, lm1, mu1, L1);

   i2x = (i2x % 2);
   i1x = (i1x % 2);
   // note that in order to get f77 values on needs to substitute:
   // K2S <-- k2start - 2
   // K1S <-- k1start - 2
   i2y = 2 * lm2 + 4 * mu2 + 6 * (L2 + k2start - 2) - i2y;
   i1y = 2 * lm1 + 4 * mu1 + 6 * (L1 + k1start - 2) - i1y;

   std::vector<double> ds1a(k3max * k2max * k1max, 0.0);
   std::vector<double> ds2a(k2max * k1max, 0.0);
   std::vector<double> dt1a(k1max, 0.0);
   std::vector<double> dt2a(k2max, 0.0);

   size_t index_dewu3 = 0;
   for (size_t i = 0; i < j1ta_j2ta_iea.size(); ++i, index_dewu3 += rhomax) {
      int j1t, j2t, ie2;
      std::tie(j1t, ie2, j2t) = j1ta_j2ta_iea[i];

      // This could happen -1000 to be exact... 
      if (j1t < 0) {
         continue;
      }
      int ie1 = ie3 - ie2;
      int i2z = (i2y - ie2) / 3;
      int i1z = (i1y - ie1) / 3;

      int mm2ta = std::min(j1t + m3t, j2t) + 1;
      int mm2tb = std::min(j1t - m3t, j2t);
      mm2tb = mm2ta + mm2tb;

      std::fill(ds1a.begin(), ds1a.end(), 0.0);
      int K3 = k3start;
      for (int iK3 = 0; iK3 < k3max; ++iK3, K3 += 2) {
         int mm2a = std::min(L1 + K3, L2) + 1;
         int mm2b = std::min(L1 - K3, L2);
         mm2b = mm2a + mm2b;

         for (int mm2 = 1; mm2 <= mm2b; ++mm2) {
            int m2 = mm2a - mm2;
            // IF(IABS(M2).GT.J2T)GO TO 120
            if (std::abs(m2) > j2t) {
               continue;
            }
            int i2s = j2t + m2;
            // IF(2*(I2S/2).NE.I2S)GO TO 120
            if (i2s % 2) {
               continue;
            }
            int m1 = K3 - m2;
            // IF(IABS(M1).GT.J1T)GO TO 120
            if (std::abs(m1) > j1t) {
               continue;
            }
            int i1s = j1t + m1;
            // IF(2*(I1S/2).NE.I1S)GO TO 120
            if (i1s % 2) {
               continue;
            }
            std::fill(ds2a.begin(), ds2a.end(), 0.0);
            for (int mm2t = 1; mm2t <= mm2tb; mm2t += 2) {
               int m2t = mm2ta - mm2t;

               // IF(M2T.NE.0)GO TO 55
               if (m2t == 0) {
                  // IF(4*(I2S/4).NE.I2S)GO TO 110
                  if (i2s % 4) {
                     continue;
                  }
               }
               // 55 IF(M2.NE.0)GO TO 60
               if (m2 == 0) {
                  int is = i2z + m2t;
                  // IF(4*(IS/4).NE.IS)GO TO 110
                  if (is % 4) {
                     continue;
                  }
                  // IS=(J2T+M2T)/2
                  // IF(2*(IS/2).NE.IS)GO TO 110
                  is = (j2t + m2t) / 2;
                  if (is % 2) {
                     continue;
                  }
               }
               // 60 M1T=M3T-M2T
               int m1t = m3t - m2t;
               // IF(M1T.NE.0)GO TO 65
               if (m1t == 0) {
                  // IF(4*(I1S/4).NE.I1S)GO TO 110
                  if (i1s % 4) {
                     continue;
                  }
               }
               // 65 IF(M1.NE.0)GO TO 70
               if (m1 == 0) {
                  // IS=I1Z+M1T
                  int is = i1z + m1t;
                  // IF(4*(IS/4).NE.IS)GO TO 110
                  if (is % 4) {
                     continue;
                  }
                  // IS=(J1T+M1T)/2
                  is = (j1t + m1t) / 2;
                  // IF(2*(IS/2).NE.IS)GO TO 110
                  if (is % 2) {
                     continue;
                  }
               }
               ComputeGGEkLMbrackets(i1x, I1, J1, lm1, mu1, ie1, j1t, m1t, k1start, L1, m1, O1,
                                     dt1a);
               ComputeGGEkLMbrackets(i2x, I2, J2, lm2, mu2, ie2, j2t, m2t, k2start, L2, m2, O2,
                                     dt2a);
               int nM1T = -m1t;
               int nM2T = -m2t;
               int nM3T = -m3t;
               double dc = dwr3(j1t, j2t, j3t, nM1T, nM2T, nM3T);
               size_t index = 0;
               for (int k2 = 0; k2 < k2max; ++k2) {
                  for (int k1 = 0; k1 < k1max; ++k1) {
                     ds2a[index++] += dc * dt1a[k1] * dt2a[k2];
                  }
               }
            }  // 110 mm2t
            int LL1 = 2 * L1;
            int LL2 = 2 * L2;
            int LL3 = 2 * L3;
            int MM1 = 2 * m1;
            int MM2 = 2 * m2;
            int KK3 = 2 * K3;
            double dc = dwr3(LL1, LL2, LL3, MM1, MM2, KK3);
            // pointer to ds1a array with data for current K3 value
            size_t index = iK3 * k2max * k1max;
            size_t k2k1_index = 0;
            for (int k2 = 0; k2 < k2max; ++k2) {
               for (int k1 = 0; k1 < k1max; ++k1) {
                  ds1a[index++] += dc * ds2a[k2k1_index++];
               }
            }
         }  // 120 mm2
      }     // 120 iK3
      size_t ik3k2k1rho = 0;
      size_t ik3k2k1 = 0;
      for (int k3 = 0; k3 < k3max; ++k3) {
         for (int k2 = 0; k2 < k2max; ++k2) {
            for (int k1 = 0; k1 < k1max; ++k1, ++ik3k2k1) {
               for (int rho = 0; rho < rhomax; ++rho) {
                  su3cgs[ik3k2k1rho++] += dewu3[index_dewu3 + rho] * ds1a[ik3k2k1];
               }
            }
         }
      }
   }  // i --> {jj1t, ie2, jj2t}

   // orthogonalize { < k1; k2 || K3> } --> { < k1, k2 || k3> }
   size_t n210 = k2max * k1max * rhomax;
   size_t ik3k2k1rho = 0;
   for (int iK3 = 0; iK3 < k3max; ++iK3) {
      size_t ik2k1rho = 0;
      for (int k2 = 0; k2 < k2max; ++k2) {
         for (int k1 = 0; k1 < k1max; ++k1) {
            for (int rho = 0; rho < rhomax; ++rho, ++ik2k1rho, ++ik3k2k1rho) {
               double ds = 0;
               size_t ji_index = iK3;
               size_t isu3cg = ik2k1rho;
               for (int jk = 0; jk <= iK3; ++jk) {
                  ds += O3[ji_index] * su3cgs[isu3cg];
                  isu3cg += n210;
                  ji_index += k3max;
               }

               su3cgs[ik3k2k1rho] = ds;
            }
         }
      }
   }
}

void ShowRangeErrorTerminate(int lm, int mu)
{
   std::cerr << "Error in input parameters of SU(3)>SO(3) coeffs: (" << lm << " " << mu
             << ") is beyond the current limit (lm + mu) <= " << su3::max_lmmu << std::endl;
   exit(EXIT_FAILURE);
}

void wu3r3w(int lm1, int mu1, int lm2, int mu2, int lm3, int mu3, int L1, int L2,
                           int L3, int k1max, int k2max, int k3max, int rhomax, std::vector<double>& su3cgs) {
   if (lm1 + mu1 > su3::max_lmmu)
   {
      ShowRangeErrorTerminate(lm1, mu1);
   }

   if (lm2 + mu2 > su3::max_lmmu)
   {
      ShowRangeErrorTerminate(lm2, mu2);
   }
   
   if (lm3 + mu3 > su3::max_lmmu)
   {
      ShowRangeErrorTerminate(lm3, mu3);
   }

   assert((std::abs(L1-L2)) <= L3 && L3 <= (L1 + L2));
   su3cgs.assign(k3max * k2max * k1max * rhomax, 0.0);
   int I1, J1, I2, J2, I3, J3;

   setU3ExtremeState(lm1, mu1, I1, J1, 0, 1);
   setU3ExtremeState(lm2, mu2, I2, J2, 0, 1);
   setU3ExtremeState(lm3, mu3, I3, J3, 0, 1);

   std::vector<double> O1(k1max * k1max, 0.0);
   std::vector<double> O2(k2max * k2max, 0.0);
   std::vector<double> O3(k3max * k3max, 0.0);

   // compute orthogonalization matrices 
   Conmat(I1, J1, lm1, mu1, k1max, L1, O1.data());
   Conmat(I2, J2, lm2, mu2, k2max, L2, O2.data());
   Conmat(I3, J3, lm3, mu3, k3max, L3, O3.data());

   int nec;
   std::vector<int> j1ta;
   std::vector<int> j2ta;
   std::vector<int> ie2a;
   std::vector<double> dewu3;
   xewu3(lm1, mu1, lm2, mu2, lm3, mu3, I3, nec, rhomax, j1ta, j2ta, ie2a, dewu3);
   assert(j1ta.size() == j2ta.size()); 
   assert(j2ta.size() == ie2a.size()); 
   assert(ie2a.size()*rhomax == dewu3.size());

   std::vector<std::tuple<int, int, int>> j1ta_j2ta_iea;
   j1ta_j2ta_iea.reserve(j1ta.size());
   for (size_t i = 0; i < j1ta.size(); ++i) {
      j1ta_j2ta_iea.push_back(std::make_tuple(j1ta[i], ie2a[i], j2ta[i]));
   }

   xwu3r3(I1, J1, lm1, mu1, L1, O1, I2, J2, lm2, mu2, L2, O2, I3, J3, lm3, mu3, L3, O3, k3max,
          k2max, k1max, rhomax, dewu3, j1ta_j2ta_iea, su3cgs);
}

void wu3r3w(int lm1, int mu1, int lm2, int mu2, int lm3, int mu3, int L1, int L2, int L3, std::vector<double>& su3cgs) {
   int k1max = su3::kmax(lm1, mu1, L1);
   int k2max = su3::kmax(lm2, mu2, L2);
   int k3max = su3::kmax(lm3, mu3, L3);
   int rhomax = su3::mult(lm1, mu1, lm2, mu2, lm3, mu3);

   if (!k1max) {
      std::cerr << "L1:" << L1 << " does not belong to (" << lm1 << " " << mu1 << ") SU(3) irrep."
                << std::endl;
      return;
   }
   if (!k2max) {
      std::cerr << "L2:" << L1 << " does not belong to (" << lm2 << " " << mu2 << ") SU(3) irrep."
                << std::endl;
      return;
   }
   if (!k3max) {
      std::cerr << "L3:" << L3 << " does not belong to (" << lm3 << " " << mu3 << ") SU(3) irrep."
                << std::endl;
      return;
   }
   wu3r3w(lm1, mu1, lm2, mu2, lm3, mu3, L1, L2, L3, k1max, k2max, k3max, rhomax, su3cgs);
}

} // namespace su3
