#include <cmath>
#include <iostream>
#include <vector>

#include <boost/multiprecision/cpp_bin_float.hpp>
#include <boost/multiprecision/float128.hpp>

extern "C"
{
#include <wigxjpf.h>
}

#include <su3.h>

namespace su3
{

// Racah W-coefficient
double drr3(int jj1, int jj2, int ll2, int ll1, int jj3, int ll3)
{
   int phase = ((jj1 + jj2 + ll1 + ll2) % 4) ? -1 : 1;
   return phase * wig6jj(jj1, jj2, jj3, ll1, ll2, ll3);
}

void dlut(int ma, int na, std::vector<int>& ia, std::vector<double>& da, int md) {
   int ibig;
   int la = std::min(ma, na);
   int lq = -md;
   for (int l = 0; l < la; ++l) {
      if (l == (la - 1) && ma <= na) {
         return;
      }
      lq = lq + md;
      double dbig = 0.0;
      for (int i = l; i < ma; ++i) {
         int il = i + lq;
         if ((std::fabs(da[il]) - std::fabs(dbig)) <= 0) {
            continue;
         }
         ibig = i;
         dbig = da[il];
      }
      std::swap(ia[l], ia[ibig]);
      int jq = -md;
      for (int j = 0; j < na; ++j) {
         jq = jq + md;
         int lj = l + jq;
         int ibigj = ibig + jq;
         std::swap(da[lj], da[ibigj]);
      }
      int k = l + 1;
      for (int i = k; i < ma; ++i) {
         int il = i + lq;
         da[il] = da[il] / dbig;
      }
      if (l == (la - 1)) {
         return;
      }
      for (int i = k; i < ma; ++i) {
         int il = i + lq;
         int jq = lq;
         for (int j = k; j < na; ++j) {
            jq = jq + md;
            int ij = i + jq;
            int lj = l + jq;
            da[ij] = da[ij] - da[il] * da[lj];
         }
      }
   }
}

void dbsr(int ma, std::vector<double>& da, std::vector<double>& db, std::vector<double>& dc,
          int md) {
   int na;
   int ij;
   dc[0] = db[0];
   for (int i = 2; i <= ma; ++i) {
      na = i - 1;
      double dsum = 0;
      int jq = -md;
      for (int j = 1; j <= na; ++j) {
         jq = jq + md;
         ij = i + jq;
         dsum = dsum + da[ij - 1] * dc[j - 1];
      }
      dc[i - 1] = db[i - 1] - dsum;
   }

   int mama = ij + md;
   db[ma - 1] = dc[ma - 1] / da[mama - 1];
   int iq = na * md;
   for (int ip = 1; ip <= na; ++ip) {
      iq = iq - md;
      int i = ma - ip;
      int ii = i + iq;
      double dsum = 0;
      int jq = ma * md;
      for (int jp = 1; jp <= ip; ++jp) {
         jq = jq - md;
         int j = ma - jp + 1;
         ij = i + jq;
         dsum = dsum + da[ij - 1] * db[j - 1];
         db[i - 1] = (dc[i - 1] - dsum) / da[ii - 1];
      }
   }
}


// Simplfified interface to computation of U6 SU(3) Racah coefficients.
//
// Datatype of wru3_internal is set according to required precision fo a given set of 
// CGs <(lm2 mu2) ; (lm3 mu3) || (lm23 mu23)>rho23
void wru3(int lm1, int mu1, int lm2, int mu2, int lm, int mu, int lm3, int mu3, int lm12, int mu12,
          int lm23, int mu23, int rho12max, int rho12_3max, int rho23max, int rho1_23max,
          std::vector<double>& dru3) {
   assert(rho12max <= 35 && rho12_3max <= 35 && rho23max <= 35 && rho1_23max <= 35);
   if (rho23max <= 12) // rhomax=12 => max|Oij| = 1e-15
   {
      wru3_internal<double>(lm1, mu1, lm2, mu2, lm, mu, lm3, mu3, lm12, mu12, lm23, mu23, rho12max,
                            rho12_3max, rho23max, rho1_23max, dru3);
   }
   else if (rho23max <= 15) // rhomax=15 => max|Oij| = 1e-15
   {
#ifdef HAVE_80BIT_LONG_DOUBLE
      wru3_internal<long double>(lm1, mu1, lm2, mu2, lm, mu, lm3, mu3, lm12, mu12, lm23, mu23,
                                 rho12max, rho12_3max, rho23max, rho1_23max, dru3);
#else
      wru3_internal<boost::multiprecision::cpp_bin_float_double_extended>(
          lm1, mu1, lm2, mu2, lm, mu, lm3, mu3, lm12, mu12, lm23, mu23, rho12max, rho12_3max,
          rho23max, rho1_23max, dru3);
#endif
   }
   else if (rho23max <= 27) // rhomax=27 => max|Oij| = 1e-15
   {
#ifdef HAVE_FLOAT128
      wru3_internal<boost::multiprecision::float128>(lm1, mu1, lm2, mu2, lm, mu, lm3, mu3, lm12,
                                                     mu12, lm23, mu23, rho12max, rho12_3max,
                                                     rho23max, rho1_23max, dru3);
#else
      wru3_internal<boost::multiprecision::cpp_bin_float_quad>(lm1, mu1, lm2, mu2, lm, mu, lm3, mu3, lm12,
                                                     mu12, lm23, mu23, rho12max, rho12_3max,
                                                     rho23max, rho1_23max, dru3);
#endif      
   }
   else
   {
      wru3_internal<boost::multiprecision::cpp_bin_float_oct>(lm1, mu1, lm2, mu2, lm, mu, lm3, mu3, lm12,
                                                     mu12, lm23, mu23, rho12max, rho12_3max,
                                                     rho23max, rho1_23max, dru3);
   }
}

// Simplfified interface to computation of U6 SU(3) Racah coefficients.
//
// Datatype of wzu3_internal is set according to required precision fo a given set of 
// CGs <(lm2 mu2) ; (lm1 mu1) || (lm12 mu12)>rho12
void wzu3(int lm1, int mu1, int lm2, int mu2, int lm, int mu, int lm3, int mu3, int lm12, int mu12,
          int lm23, int mu23, int rho12max, int rho12_3max, int rho23max, int rho1_23max,
          std::vector<double>& dzu3) {
   assert(rho12max <= 35 && rho12_3max <= 35 && rho23max <= 35 && rho1_23max <= 35);
   if (rho12max <= 12) // rhomax=12 => max|Oij| = 1e-15
   {
      wzu3_internal<double>(lm1, mu1, lm2, mu2, lm, mu, lm3, mu3, lm12, mu12, lm23, mu23, rho12max,
                            rho12_3max, rho23max, rho1_23max, dzu3);
   }
   else if (rho12max <= 15) // rhomax=15 => max|Oij| = 1e-15
   {
#ifdef HAVE_80BIT_LONG_DOUBLE
      wzu3_internal<long double>(lm1, mu1, lm2, mu2, lm, mu, lm3, mu3, lm12, mu12, lm23, mu23,
                                 rho12max, rho12_3max, rho23max, rho1_23max, dzu3);
#else
      wzu3_internal<boost::multiprecision::cpp_bin_float_double_extended>(
          lm1, mu1, lm2, mu2, lm, mu, lm3, mu3, lm12, mu12, lm23, mu23, rho12max, rho12_3max,
          rho23max, rho1_23max, dzu3);
#endif
   }
   else if (rho12max <= 27) // rhomax=27 => max|Oij| = 1e-15
   {
#ifdef HAVE_FLOAT128
      wzu3_internal<boost::multiprecision::float128>(lm1, mu1, lm2, mu2, lm, mu, lm3, mu3, lm12,
                                                     mu12, lm23, mu23, rho12max, rho12_3max,
                                                     rho23max, rho1_23max, dzu3);
#else
      wzu3_internal<boost::multiprecision::cpp_bin_float_quad>(lm1, mu1, lm2, mu2, lm, mu, lm3, mu3, lm12,
                                                     mu12, lm23, mu23, rho12max, rho12_3max,
                                                     rho23max, rho1_23max, dzu3);
#endif      
   }
   else
   {
      wzu3_internal<boost::multiprecision::cpp_bin_float_oct>(lm1, mu1, lm2, mu2, lm, mu, lm3, mu3, lm12,
                                                     mu12, lm23, mu23, rho12max, rho12_3max,
                                                     rho23max, rho1_23max, dzu3);
   }
}

} // namespace su3
