#ifndef SU3_SU3_H
#define SU3_SU3_H

#include "su3_config.h"

#include "impl/common.h"
#include "impl/conmat.h"
#include "impl/dtu3r3.h"
#include "impl/init.h"
#include "impl/su3_so3.h"
#include "impl/su3_su2u1.h"
#include "impl/su3_u6z6.h"
#include "impl/su3_u9.h"
#include "impl/utils.h"

#endif
