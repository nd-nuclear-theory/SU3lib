#ifndef SU3_COMMON_H
#define SU3_COMMON_H

#include <array>
#include <cassert>
#include <vector>

#include <boost/math/special_functions/binomial.hpp>

namespace su3
{

extern std::array<long double, 176> one_over_nfact;

inline int n_k(int n, int k)
{
   return n * (n + 1) / 2 + k;
}

template <typename T>
class bc
{
   static std::vector<T>& v_bino_() noexcept
   {
      static std::vector<T> v_;
      return v_;
   }

   static std::vector<T>& v_binv_() noexcept
   {
      static std::vector<T> v_;
      return v_;
   }

public:
   static void init(int N)
   {
      // nelems: number of (0,0) .... (N,N) binomial numbers
      const size_t nelems = (N + 1) * (N + 2) / 2;
      v_bino_().reserve(nelems);
      v_binv_().reserve(nelems);
/*
      for (int n = 0; n <= N; n++)
      {
         for (int k = 0; k <= n; k++)
         {
            v_bino_().push_back(boost::math::binomial_coefficient<T>(n, k));
            v_binv_().push_back(T{1.0} / v_bino_().back());
         }
      }
*/
      // (0 over 0) = 1:
      v_bino_().push_back(T{1.0});
      v_binv_().push_back(T{1.0});

      for (int n = 1; n <= N; n++)
      {
         // (n over 0) = 1:
         v_bino_().push_back(T{1.0});
         v_binv_().push_back(T{1.0});

         // (n over 1,...,n-1):
         for (int k = 1; k < n; k++) {
            // recursive formula:
            v_bino_().push_back(bino(n - 1, k - 1) + bino(n - 1, k));
            v_binv_().push_back(T{1.0} / v_bino_().back());
         }

         // (n over n) = 1:
         v_bino_().push_back(T{1.0});
         v_binv_().push_back(T{1.0});
      }
   }

   static T bino(int n, int k)
   {
      assert(v_bino_().size() > 0);
      assert(n_k(n, k) < v_bino_().size());

      return v_bino_()[n_k(n, k)];
   }

   static T binv(int n, int k)
   {
      assert(v_binv_().size() > 0);
      assert(n_k(n, k) < v_binv_().size());

      return v_binv_()[n_k(n, k)];
   }

   static T bino(int nk)
   {
      assert(v_bino_().size() > 0);
      assert(nk < v_bino_().size());

      return v_bino_()[nk];
   }

   static T binv(int nk)
   {
      assert(v_binv_().size() > 0);
      assert(nk < v_binv_().size());

      return v_binv_()[nk];
   }
};

template <typename T>
T bino(int n, int k) 
{
   return bc<T>::bino(n, k);
}

template <typename T>
T binv(int n, int k) 
{
   return bc<T>::binv(n, k);
}

template <typename T>
T bino(int nk) 
{
   return bc<T>::bino(nk);
}

template <typename T>
T binv(int nk) 
{
   return bc<T>::binv(nk);
}

void internal_init();

} // namespace su3

#endif
