#include <algorithm>
#include <cassert>
#include <tuple>
#include <vector>

#include <su3.h>

namespace su3
{

void couple(int lm1, int mu1, int lm2, int mu2, std::vector<std::tuple<int, int, int>>& ir3) {
   ir3.reserve(3 * (lm1 + mu1 + lm2 + mu2));

   int ix_max = std::min(mu1, lm2);
   for (int ix = 0; ix <= ix_max; ++ix) {
      int iy_max = std::min(lm1, lm2 + mu2 - ix);
      for (int iy = 0; iy <= iy_max; ++iy) {
         int iz_min = std::max(0, mu2 - mu1 + ix - iy);
         int iz_max = std::min(mu2, lm2 + mu2 - ix - iy);
         for (int iz = iz_min; iz <= iz_max; ++iz) {
            int lm3 = lm1 + lm2 + mu2 - 2 * iy - ix - iz;
            int mu3 = mu1 - mu2 - ix + iy + 2 * iz;
            int ip = (2 * (mu3 - mu1) + lm3 - lm1 - lm2 + mu2) / 3;
            int in = (2 * (lm1 + lm2 - lm3) + mu1 + mu2 - mu3) / 3;
            int irho = ix - std::max({0, -ip, in - lm1}) + 1;
            if (irho != 1) {
               continue;
            }
            ir3.push_back(std::make_tuple(su3::mult(lm1, mu1, lm2, mu2, lm3, mu3), lm3, mu3));
         }
      }
   }
}

// ifix = 0 jfix = 1 --> G_E = G_LW'
// ifix --> I jfix --> J if (lm >= mu)
// use its conjugate of (lm < mu)
void setU3ExtremeState(int lm, int mu, int& I, int& J, int ifix, int jfix) {
   I = ifix;
   J = jfix;
   if (lm >= mu) return;
   I = 1 - ifix;
   J = 1 - jfix;
   return;
}

int mult(int lm1, int mu1, int lm2, int mu2, int lm3, int mu3) {
   int result = 0;
   int phase = lm1 + lm2 - lm3 - mu1 - mu2 + mu3;

   if (phase % 3 == 0) {
      int redphase;
      int l1, l2, l3, m1, m2, m3;

      redphase = phase / 3;
      if (phase >= 0) {
         l1 = lm1;
         l2 = lm2;
         l3 = lm3;
         m1 = mu1;
         m2 = mu2;
         m3 = mu3;
      } else {
         l1 = mu1;
         l2 = mu2;
         l3 = mu3;
         m1 = lm1;
         m2 = lm2;
         m3 = lm3;
         redphase = -redphase;
      }
      phase = redphase + m1 + m2 - m3;

      int invl1 = std::min(l1 - redphase, m2);
      if (invl1 < 0) {
         return result;
      }

      int invl2 = std::min(l2 - redphase, m1);
      if (invl2 < 0) {
         return result;
      }
      result = std::max(std::min(phase, invl2) - std::max(phase - invl1, 0) + 1, 0);
   }
   return result;
}

// epsilon_E 2Lambda_E 2M_E
std::tuple<int, int, int> GetExtremalStateLabels(int I, int J, int lm, int mu)
{
   int eps, jjt, mmt;
   if (I == 0)
   {
      eps = 2 * lm + mu;
      jjt = mu;
      mmt = -mu;
   } else {
      eps = -(lm + 2 * mu);
      jjt = mmt = lm;
   }
   if (I != J) {
      mmt = -mmt;
   }
   return std::make_tuple(eps, jjt, mmt);
}

// just a helper function
inline int KMIN(int lm, int mu, int L) {
     return (lm % 2) + 2 * (std::max(0, (L - mu) / 2) + ((lm + 1) % 2) * (std::abs(L - mu) % 2));
}

// returns Kmin value, see formulas 4(a) and 4(b) in Draayer & Akyiama JMP73.
int Kmin(int I, int J, int lm, int mu, int L) {
   if (I == 0) {
      return KMIN(mu, lm, L);
   } else {
      return KMIN(lm, mu, L);
   }
}

} // namespace su3
