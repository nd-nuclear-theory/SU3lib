TARGET = src/libSU3.a

.PHONY: all
all: $(TARGET) tools

$(TARGET):
	$(MAKE) -C src

.PHONY: install
install: all
	mkdir -p lib
	cp $(TARGET) lib/

.PHONY: tools
tools: $(TARGET)
	$(MAKE) -C tools

.PHONY: clean
clean:
	$(MAKE) -C src clean
	$(MAKE) -C tools clean
