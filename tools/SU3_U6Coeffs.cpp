#include <cassert>
#include <iomanip>
#include <iostream>

#include <su3.h>

int main(int argc, char** argv) {
   su3::init();

   int lm1, mu1, lm2, mu2, lm, mu, lm3, mu3, lm12, mu12, lm23, mu23;
   std::cout << "Computation of U6 recoupling coefficients U((lm1 mu1) (lm2 mu2) (lm mu) (lm3 "
                "mu3);(lm12 mu12)r12 r12_3 (lm23 lm23)r23 r1_23)."
             << std::endl;
   std::cout << "Enter (lm1 mu1) " << std::endl;
   std::cin >> lm1 >> mu1;
   std::cout << "Enter (lm2 mu2) " << std::endl;
   std::cin >> lm2 >> mu2;
   std::cout << "Enter (lm mu) " << std::endl;
   std::cin >> lm >> mu;
   std::cout << "Enter (lm3 mu3) " << std::endl;
   std::cin >> lm3 >> mu3;
   std::cout << "Enter (lm12 mu12) " << std::endl;
   std::cin >> lm12 >> mu12;
   std::cout << "Enter (lm23 mu23) " << std::endl;
   std::cin >> lm23 >> mu23;

   int rho12max = su3::mult(lm1, mu1, lm2, mu2, lm12, mu12);
   int rho12_3max = su3::mult(lm12, mu12, lm3, mu3, lm, mu);
   int rho23max = su3::mult(lm2, mu2, lm3, mu3, lm23, mu23);
   int rho1_23max = su3::mult(lm1, mu1, lm23, mu23, lm, mu);

	if (!rho12max) 
	{
		std::cerr << "(" << lm1 << " " << mu1 << ") x (" << lm2 << " " << mu2 << ")";
      std::cerr << " does not couple to (" << lm12 << " " << mu12 << ")" << std::endl;
      return EXIT_FAILURE;
	}
	if (!rho23max) 
	{
		std::cerr << "(" << lm2 << " " << mu2 << ") x (" << lm3 << " " << mu3 << ")";
      std::cerr << " does not couple to (" << lm23 << " " << mu23 << ")" << std::endl;
      return EXIT_FAILURE;
	}
	if (!rho12_3max) 
	{
		std::cerr << "(" << lm12 << " " << mu12 << ") x (" << lm3 << " " << mu3 << ")";
      std::cerr << " does not couple to (" << lm << " " << mu << ")" << std::endl;
      return EXIT_FAILURE;
	}
	if (!rho1_23max) 
	{
		std::cerr << "(" << lm1 << " " << mu1 << ") x (" << lm23 << " " << mu23 << ")";
      std::cerr << " does not couple to (" << lm << " " << mu << ")" << std::endl;
      return EXIT_FAILURE;
	}

   std::vector<double> dru3;
   su3::wru3(lm1, mu1, lm2, mu2, lm, mu, lm3, mu3, lm12, mu12, lm23, mu23, rho12max, rho12_3max,
        rho23max, rho1_23max, dru3);

   std::cout << std::setprecision(8);
   std::cout << "U((l1 m1) (l2 m2) (lm mu) (l3 m3);(l12 m12)r12 r12_3 (l23 m23)r23 r1_23)" << std::endl;
   std::cout << std::fixed << std::setw(4) << lm1;
   std::cout << std::fixed << std::setw(3) << mu1;

   std::cout << std::fixed << std::setw(5) << lm2;
   std::cout << std::fixed << std::setw(3) << mu2;

   std::cout << std::fixed << std::setw(5) << lm;
   std::cout << std::fixed << std::setw(3) << mu;

   std::cout << std::fixed << std::setw(5) << lm3;
   std::cout << std::fixed << std::setw(3) << mu3;
   
   std::cout << std::fixed << std::setw(6) << lm12;
   std::cout << std::fixed << std::setw(3) << mu12;

   std::cout << std::fixed << std::setw(16) << lm23;
   std::cout << std::fixed << std::setw(3) << mu23;
   std::cout << std::endl;
   
	int na = rho12max;
	int nb = rho12_3max*na;
	int nc = rho23max*nb;
   size_t index = 0;
   for (size_t rho1_23 = 0; rho1_23 < rho1_23max; ++rho1_23) {
      for (size_t rho23 = 0; rho23 < rho23max; ++rho23) {
         for (size_t rho12_3 = 0; rho12_3 < rho12_3max; ++rho12_3) {
            for (size_t rho12 = 0; rho12 < rho12max; ++rho12) {
					int i = rho12 + rho12_3*na + rho23*nb + rho1_23*nc; 
               assert(i == index);
               std::cout << std::fixed << std::setw(44) << rho12 << std::setw(4) << rho12_3;
               std::cout << std::fixed << std::setw(15) << rho23 << std::setw(4) << rho1_23 << "\t" << dru3[index] << std::endl;

               index++;
            }
         }
      }
   }

   su3::finalize();

   return EXIT_SUCCESS;
}

