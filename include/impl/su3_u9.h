#ifndef SU3_SU3_U9_H
#define SU3_SU3_U9_H

#include <vector>

namespace su3
{

// Compute 9-(lm mu) recoupling coefficients
void wu39lm(int lm1, int mu1, int lm2, int mu2, int lm12, int mu12, int lm3, int mu3, int lm4,
            int mu4, int lm34, int mu34, int lm13, int mu13, int lm24, int mu24, int lm, int mu,
            std::vector<double>& su39lm);

} // namespace su3

#endif
