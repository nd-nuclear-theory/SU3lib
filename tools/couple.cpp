#include <iomanip>
#include <iostream>

#include <su3.h>

int main(int argc, char** argv) {
   su3::init(250);
   int lm1, mu1, lm2, mu2, lm3, mu3;
   int L1sel, L2sel, L3sel;

   std::cout << "Generate product of two SU(3) irreps (lm1,mu1) x (lm2, mu2)."<< std::endl;

   std::cout << "Enter (lm1 mu1) " << std::endl;
   std::cin >> lm1 >> mu1;

   std::cout << "Enter (lm2 mu2) " << std::endl;
   std::cin >> lm2 >> mu2;

   std::vector<std::tuple<int, int, int>> product_irreps; // (rho,lm,mu) 

   su3::couple(lm1,mu1,lm2,mu2,product_irreps);
   for(const auto& [rho,lm,mu] : product_irreps)
      {
         std::cout<<rho<<"("<<lm<<","<<mu<<")"<<std::endl;
      }
   su3::finalize();
}
